package layouts.sourceit.com.weatherforecast;

interface Tools {
    void initToolbar();

    void setupDrawerLayout();
}
