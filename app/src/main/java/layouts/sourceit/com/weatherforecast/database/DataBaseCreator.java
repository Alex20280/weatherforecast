package layouts.sourceit.com.weatherforecast.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.TABLE_NAME;

public class DataBaseCreator extends SQLiteOpenHelper {

    public static final String DB_NAME = "database";
    public static final int DB_VERSION = 1;

    public static class MyResponse implements BaseColumns {
        public static final String TABLE_NAME = "t_name";
        public static final String QUERY = "query";
        public static final String DATE = "date";
        public static final String SUNRISE = "sunrise";
        public static final String SUNSET = "sunset";
        public static final String MOONRISE = "moonrise";
        public static final String MOONSET = "moonset";
        public static final String CHANCEOFRAIN = "chance_of_rain";
        public static final String PRESSURE = "pressure";
        public static final String TEMPC = "temp_c";
        public static final String FEELSLIKEC = "feelslike_c";
        public static final String WINDDIR16POINT = "winddir16_point";
        public static final String WINDSPEEDKMPH = "windspeedKmph";
        public static final String HUMIDITY = "humidity";
        public static final String VALUE = "value";
        public static final String ICON = "url_icon";
    }

    static String SCRIPT_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            MyResponse._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            MyResponse.QUERY + " TEXT, " +
            MyResponse.DATE + " TEXT" +
            MyResponse.SUNRISE + " TEXT" +
            MyResponse.SUNSET + " TEXT" +
            MyResponse.MOONRISE + " TEXT" +
            MyResponse.MOONSET + " TEXT" +
            MyResponse.CHANCEOFRAIN + " TEXT" +
            MyResponse.PRESSURE + " TEXT" +
            MyResponse.TEMPC + " TEXT" +
            MyResponse.FEELSLIKEC + " TEXT" +
            MyResponse.WINDDIR16POINT + " TEXT" +
            MyResponse.WINDSPEEDKMPH + " TEXT" +
            MyResponse.HUMIDITY + " TEXT" +
            MyResponse.VALUE + " TEXT" +
            MyResponse.ICON + " TEXT" +
            ");";

    public DataBaseCreator(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SCRIPT_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}
