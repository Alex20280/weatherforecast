package layouts.sourceit.com.weatherforecast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.RemoteViews;

import com.squareup.picasso.Picasso;

import static android.content.Context.NOTIFICATION_SERVICE;
import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class NetworkChangeReceiver extends BroadcastReceiver {

    public static final int ID = 101;
    public static final int NOTIFY_ID = 500;

    @Override
    public void onReceive(Context context, Intent intent) {

        if (isNetworkAvailable(context)) Notification(context);
    }

    public void Notification(Context context) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        notificationIntent.addFlags(FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(context,
                ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationManager nm = (NotificationManager)
                context.getSystemService(NOTIFICATION_SERVICE);

        Notification.Builder builder = new Notification.Builder(context);
        builder.setContentIntent(contentIntent)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(false)
                .setContentTitle("Weather Description")
                .setLights(Color.BLUE, 500, 500)
                .setContentText("")
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});
        Notification n = builder.build();
        nm.notify(NOTIFY_ID, n);

        final RemoteViews contentView = n.contentView;
        final int iconId = android.R.id.icon;
        Picasso.with(context).load("http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png").centerInside().resize(250, 250).into(contentView, iconId, NOTIFY_ID, n);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }
}
