package layouts.sourceit.com.weatherforecast.font;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.widget.TextView;

import java.util.Hashtable;

public class FontUtils {

    private static final Hashtable<String, Typeface> cache = new Hashtable<>();
    private Typeface architectsdaughter;
    private Typeface lettergothicstd;
    private Typeface pacificoregular;
    private Typeface loraregular;
    private Typeface sourcesansproregular;

    public FontUtils(Context context) {
        lettergothicstd = get(context, "fonts/lettergothicstd");
        loraregular = get(context, "fonts/loraregular");
        sourcesansproregular = get(context, "fonts/sourcesansproregular");
    }

    private static Typeface get(Context c, String name) {
        synchronized (cache) {
            if (!cache.containsKey(name)) {
                Typeface t = Typeface.createFromAsset(c.getAssets(),
                        String.format("%s.ttf", name));
                cache.put(name, t);
            }
            return cache.get(name);
        }
    }

    public TextView changeFontTypeToNormal(View view) {

        TextView objTextView = (TextView) view;
        objTextView.setTypeface(this.sourcesansproregular);
        return objTextView;
    }
    public TextView changeFontTypeToNormalDesc(View view) {

        TextView objTextView = (TextView) view;
        objTextView.setTypeface(this.loraregular);
        return objTextView;
    }
    public TextView changeFontTypeToItalic(View view) {

        TextView objTextView = (TextView) view;
        objTextView.setTypeface(this.lettergothicstd);
        return objTextView;
    }
}