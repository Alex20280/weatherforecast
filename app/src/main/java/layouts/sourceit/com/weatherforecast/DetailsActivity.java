package layouts.sourceit.com.weatherforecast;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import layouts.sourceit.com.weatherforecast.font.FontUtils;
import layouts.sourceit.com.weatherforecast.models.MyResponse;

public class DetailsActivity extends AppCompatActivity implements Tools {

    public static final String KEY = "weather";
    public static final String KEY2 = "request";
    @BindView(R.id.dateTextview)
    public TextView dateTextview;
    @BindView(R.id.cityTextview)
    public TextView cityTextview;
    @BindView(R.id.tempCText)
    public TextView tempCText;
    @BindView(R.id.feelsLikeTv)
    public TextView feelsLikeTv;
    @BindView(R.id.feelsLikeTvDetails)
    public TextView feelsLikeTvDetails;
    @BindView(R.id.humidityTv)
    public TextView humidityTv;
    @BindView(R.id.humidityTvDetails)
    public TextView humidityTvDetails;
    @BindView(R.id.pressureTv)
    public TextView pressureTv;
    @BindView(R.id.pressureTvDetails)
    public TextView pressureTvDetails;
    @BindView(R.id.rainPoss)
    public TextView rainPoss;
    @BindView(R.id.rainPossDetails)
    public TextView rainPossDetails;
    @BindView(R.id.descriptionTextview)
    public TextView descriptionTextview;
    @BindView(R.id.sunriseAndSunset)
    public TextView sunriseAndSunset;
    @BindView(R.id.windText)
    public TextView windText;
    @BindView(R.id.sunriseText)
    public TextView sunriseText;
    @BindView(R.id.sunsetText)
    public TextView sunsetText;
    @BindView(R.id.moonStartText)
    public TextView moonStartText;
    @BindView(R.id.moonEndText)
    public TextView moonEndText;
    @BindView(R.id.windAndOther)
    public TextView windAndOther;
    @BindView(R.id.forecastText2)
    public TextView forecastText2;
    @BindView(R.id.humidityText)
    public TextView humidityText;
    @BindView(R.id.historicalData)
    public TextView historicalData;
    @BindView(R.id.tvMinTemp)
    public TextView tvMinTemp;
    @BindView(R.id.tvMaxTemp)
    public TextView tvMaxTemp;
    @BindView(R.id.tvMinTempValue)
    public TextView tvMinTempValue;
    @BindView(R.id.tvMaxTempValue)
    public TextView tvMaxTempValue;

    @BindView(R.id.iconWeatherTwo)
    public ImageView iconWeatherTwo;
    @BindView(R.id.windImg)
    public ImageView windImg;
    @BindView(R.id.sunriseImg)
    public ImageView sunriseImg;
    @BindView(R.id.sunsetImg)
    public ImageView sunsetImg;
    @BindView(R.id.moonStartImg)
    public ImageView moonStartImg;
    @BindView(R.id.moonEndImg)
    public ImageView moonEndImg;
    @BindView(R.id.forecastImg2)
    public ImageView forecastImg2;
    @BindView(R.id.humidity)
    public ImageView humidity;
    @BindView(R.id.zero)
    public ImageView zero;
    @BindView(R.id.zeroTwo)
    public ImageView zeroTwo;
    @BindView(R.id.detailsLayout)
    public DrawerLayout drawerLayoutdet;
    @BindView(R.id.navigation_view)
    public NavigationView navigationView;
    @BindView(R.id.content)
    public View content;
    @BindView(R.id.lin)
    public LinearLayout lin;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);
        ButterKnife.bind(this);

        if (getIntent() != null) {
            setData();
            initToolbar();
            getDayImage();
            setupDrawerLayout();
            setFonts();
        }
    }

    private void setFonts() {
        FontUtils fontUtils = new FontUtils(getApplicationContext());
        fontUtils.changeFontTypeToItalic(cityTextview);
        fontUtils.changeFontTypeToNormal(feelsLikeTv);
        fontUtils.changeFontTypeToNormal(humidityTv);
        fontUtils.changeFontTypeToNormal(pressureTv);
        fontUtils.changeFontTypeToNormal(rainPoss);
        fontUtils.changeFontTypeToNormal(dateTextview);
        fontUtils.changeFontTypeToNormalDesc(descriptionTextview);
        fontUtils.changeFontTypeToNormal(sunriseAndSunset);
        fontUtils.changeFontTypeToNormal(sunriseText);
        fontUtils.changeFontTypeToNormal(sunsetText);
        fontUtils.changeFontTypeToNormal(moonStartText);
        fontUtils.changeFontTypeToNormal(moonEndText);
        fontUtils.changeFontTypeToNormal(windAndOther);
        fontUtils.changeFontTypeToNormal(windText);
        fontUtils.changeFontTypeToNormal(forecastText2);
        fontUtils.changeFontTypeToNormal(humidityText);
        fontUtils.changeFontTypeToNormal(historicalData);
        fontUtils.changeFontTypeToNormal(tvMinTemp);
        fontUtils.changeFontTypeToNormal(tvMaxTemp);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    public void getDayImage() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 12) {
            content.setBackgroundResource(R.drawable.sunrise);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            content.setBackgroundResource(R.drawable.day);
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            content.setBackgroundResource(R.drawable.evening);
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            content.setBackgroundResource(R.drawable.night);
        }
    }
    public void imageSwitch(ImageView v) {
        String sunnyImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png";
        String clearSkyNightImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0008_clear_sky_night.png";
        String cloudWithSunImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0002_sunny_intervals.png";
        String cloudyImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0003_white_cloud.png";
        String lightRainWithSunImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0009_light_rain_showers.png";
        String lightRainImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0017_cloudy_with_light_rain.png";
        String cloudImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0004_black_low_cloud.png";
        String snowyRainImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0037_cloudy_with_sleet_night.png";
        String heavySnowImage = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0020_cloudy_with_heavy_snow.png";

        int picture = R.id.iconWeatherTwo;
        String day = "";
        String res = "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png";
        switch (day) {
            case "http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png":
                day = sunnyImage;
                break;
        }

    }
    public void setData() {
        MyResponse.Data.Weather weather = (MyResponse.Data.Weather) getIntent().getSerializableExtra(KEY);
        MyResponse.Data.Request request = (MyResponse.Data.Request) getIntent().getSerializableExtra(KEY2);
        Picasso.with(getApplicationContext())
                .load("http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0003_white_cloud.png")
                .into(iconWeatherTwo);
        dateTextview.setText(weather.date);
        cityTextview.setText(request.query);
        tempCText.setText(weather.hourly.get(0).tempC+"\u2103");
        feelsLikeTvDetails.setText(weather.hourly.get(0).FeelsLikeC+"\u2103");
        humidityTvDetails.setText(weather.hourly.get(0).humidity + "%");
        pressureTvDetails.setText(weather.hourly.get(0).pressure + " hPa");
        rainPossDetails.setText(weather.hourly.get(0).chanceofrain+"%");
        descriptionTextview.setText(weather.hourly.get(0).weatherDesc.get(0).value);
        windText.setText(weather.hourly.get(0).windspeedKmph+" km/h");
        sunriseText.setText(weather.astronomy.get(0).sunrise);
        sunsetText.setText(weather.astronomy.get(0).sunset);
        moonStartText.setText(weather.astronomy.get(0).moonrise);
        moonEndText.setText(weather.astronomy.get(0).moonset);
        forecastText2.setText(weather.hourly.get(0).winddir16Point);
        humidityText.setText(weather.hourly.get(0).humidity + "%");
    }

    @Override
    public void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_reorder);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setupDrawerLayout() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.drawer_home:
                        menuItem.setChecked(true);
                        drawerLayoutdet.closeDrawers();
                        return true;
                    case R.id.drawer_more:
                        Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                        menuItem.setChecked(true);
                        drawerLayoutdet.closeDrawers();
                        return true;
                    case R.id.drawer_settings:
                        Intent intent = new Intent(DetailsActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        menuItem.setChecked(true);
                        return true;
                }
                return onNavigationItemSelected(menuItem);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayoutdet.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}
