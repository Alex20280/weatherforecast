package layouts.sourceit.com.weatherforecast.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import layouts.sourceit.com.weatherforecast.models.MyResponse;

import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.CHANCEOFRAIN;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.DATE;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.FEELSLIKEC;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.HUMIDITY;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.ICON;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.MOONRISE;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.MOONSET;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.PRESSURE;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.QUERY;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.SUNRISE;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.SUNSET;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.TABLE_NAME;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.TEMPC;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.VALUE;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.WINDDIR16POINT;
import static layouts.sourceit.com.weatherforecast.database.DataBaseCreator.MyResponse.WINDSPEEDKMPH;

public class DataBaseMaster {
    private SQLiteDatabase database;
    private DataBaseCreator dbCreator;
    private Context context;


    private static DataBaseMaster instance;


    public DataBaseMaster(Context context) {
        dbCreator = new DataBaseCreator(context);
        database = dbCreator.getWritableDatabase();
        if (database == null || !database.isOpen()) {
            database = dbCreator.getWritableDatabase();
        }
    }
    public static DataBaseMaster getInstance(Context context) {
        if (instance == null) {
            instance = new DataBaseMaster(context);
        }
        return instance;
    }

    public void close(){
        dbCreator.close();
    }

    public long insertResponse(MyResponse myResponse) {
        ContentValues cv = new ContentValues();
        cv.put(QUERY, myResponse.data.request.get(0).query);
        cv.put(DATE, myResponse.data.weather.get(0).date);
        cv.put(SUNRISE, myResponse.data.weather.get(0).astronomy.get(0).sunrise);
        cv.put(SUNSET, myResponse.data.weather.get(0).astronomy.get(0).sunset);
        cv.put(MOONRISE, myResponse.data.weather.get(0).astronomy.get(0).moonrise);
        cv.put(MOONSET, myResponse.data.weather.get(0).astronomy.get(0).moonset);
        cv.put(CHANCEOFRAIN, myResponse.data.weather.get(0).hourly.get(0).chanceofrain);
        cv.put(PRESSURE, myResponse.data.weather.get(0).hourly.get(0).pressure);
        cv.put(TEMPC, myResponse.data.weather.get(0).hourly.get(0).tempC);
        cv.put(FEELSLIKEC, myResponse.data.weather.get(0).hourly.get(0).FeelsLikeC);
        cv.put(WINDDIR16POINT, myResponse.data.weather.get(0).hourly.get(0).winddir16Point);
        cv.put(WINDSPEEDKMPH, myResponse.data.weather.get(0).hourly.get(0).windspeedKmph);
        cv.put(HUMIDITY, myResponse.data.weather.get(0).hourly.get(0).humidity);
        cv.put(VALUE, myResponse.data.weather.get(0).hourly.get(0).weatherDesc.get(0).value);
        cv.put(VALUE, myResponse.data.weather.get(0).hourly.get(0).weatherIvonUrl.get(0).value);
        return database.insert(TABLE_NAME, null, cv);
    }

    public List<MyResponse> getResponse() {
        String query = " SELECT " +
                QUERY + ", " +
                DATE + ", " +
                SUNRISE + ", " +
                SUNSET + ", " +
                MOONRISE + ", " +
                MOONSET + ", " +
                CHANCEOFRAIN + ", " +
                PRESSURE + ", " +
                TEMPC + ", " +
                FEELSLIKEC + ", " +
                WINDDIR16POINT + ", " +
                WINDSPEEDKMPH + ", " +
                HUMIDITY + ", " +
                VALUE + ", " +
                ICON + " FROM " + TABLE_NAME;
        Cursor cursor = database.rawQuery(query, null);

        List<MyResponse> list = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            MyResponse myResponse = new MyResponse();
            myResponse.data.request.get(0).query = cursor.getString(0);
            myResponse.data.weather.get(0).date = cursor.getString(0);
            myResponse.data.weather.get(0).astronomy.get(0).sunrise = cursor.getString(0);
            myResponse.data.weather.get(0).astronomy.get(0).sunset = cursor.getString(0);
            myResponse.data.weather.get(0).astronomy.get(0).moonrise = cursor.getString(0);
            myResponse.data.weather.get(0).astronomy.get(0).moonset = cursor.getString(0);

            myResponse.data.weather.get(0).hourly.get(0).chanceofrain = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).pressure = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).tempC = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).FeelsLikeC = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).winddir16Point = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).windspeedKmph = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).humidity = cursor.getString(0);

            myResponse.data.weather.get(0).hourly.get(0).weatherDesc.get(0).value = cursor.getString(0);
            myResponse.data.weather.get(0).hourly.get(0).weatherIvonUrl.get(0).value = cursor.getString(0);

            list.add(myResponse);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

}
