package layouts.sourceit.com.weatherforecast;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import layouts.sourceit.com.weatherforecast.database.DataBaseCreator;
import layouts.sourceit.com.weatherforecast.database.DataBaseMaster;
import layouts.sourceit.com.weatherforecast.models.MyResponse;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MainActivity extends AppCompatActivity implements WeatherAdapter.WeatherClickListener, Tools {

    public static final String KEY = "weather";
    public static final String KEY2 = "request";
    public WeatherAdapter adapter;
    @BindView(R.id.list)
    RecyclerView rList;
    @BindView(R.id.mainActivityLayout)
    public DrawerLayout drawerLayout;
    @BindView(R.id.navigation_view)
    public NavigationView navigationView;
    @BindView(R.id.content)
    public View content;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    DataBaseMaster dataBaseMaster;
    DataBaseCreator dataBaseCreator;
    SQLiteDatabase sqLiteDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initToolbar();
        setupDrawerLayout();

        if (isNetworkAvailable(getApplicationContext())) {
            getData(); // Internet Connection is Present, get data from internet and save to Database

        } else {
            Toast.makeText(MainActivity.this, "No internet. Please turn it on", Toast.LENGTH_LONG).show(); // Internet connection is not present
            showDialog(); //show AlertDialog
            initializeList();
            //TODO load data from Database if not empty
        }
    }


    private void initializeList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rList.setLayoutManager(layoutManager);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(MyResponse.Data.Weather weather, MyResponse.Data.Request request) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra(KEY, weather);
        intent.putExtra(KEY2, request);
        startActivity(intent);
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    @Override
    public void initToolbar() {
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_reorder);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setupDrawerLayout() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.drawer_home:
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        return true;
                    case R.id.drawer_more:
                        Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                        menuItem.setChecked(true);
                        drawerLayout.closeDrawers();
                        return true;
                    case R.id.drawer_settings:
                        Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(intent);
                        menuItem.setChecked(true);
                        return true;
                }
                return onNavigationItemSelected(menuItem);
            }
        });
    }


    private void getData() {
        Retrofit.getData(new Callback<MyResponse>() {
            @Override
            public void success(MyResponse myResponse, Response response) {
                Toast.makeText(MainActivity.this, "OK", Toast.LENGTH_LONG).show();
                initializeList(); //ResyclerView load and show
                if (myResponse != null) {
                    adapter = new WeatherAdapter(myResponse.data.weather, myResponse.data.request, MainActivity.this, MainActivity.this);
                    rList.setAdapter(adapter);

                    //saveData(); //TODO - Save to Database

                }

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this, "NOoo", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void saveData() {
        //dataBaseCreator= new DataBaseCreator(MainActivity.this);
        MyResponse myResponse = new MyResponse();
        dataBaseMaster = DataBaseMaster.getInstance(this);
        dataBaseMaster.insertResponse(myResponse);
        Log.d("database saved", "saved");
    }

    public void showDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        AlertDialog alert = null;
        builder.setMessage("No Internet")
                .setTitle("Internet Connection")
                .setCancelable(true)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setNegativeButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                    }
                });
        alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        getData();
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}




