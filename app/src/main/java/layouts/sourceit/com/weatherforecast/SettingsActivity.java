package layouts.sourceit.com.weatherforecast;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import layouts.sourceit.com.weatherforecast.font.FontUtils;

public class SettingsActivity extends AppCompatActivity implements Tools {

    @BindView(R.id.settingsTextview)
    public TextView settingsTextview;
    @BindView(R.id.tvDays)
    public TextView tvDays;
    @BindView(R.id.tv_note)
    public TextView tv_note;
    @BindView(R.id.notificationtoggle)
    public ToggleButton notificationtoggle;
    @BindView(R.id.rb1)
    public RadioButton rb1;
    @BindView(R.id.rb2)
    public RadioButton rb2;
    @BindView(R.id.rb3)
    public RadioButton rb3;
    @BindView(R.id.settingsLayout)
    public DrawerLayout drawerLayoutset;
    @BindView(R.id.navigation_view)
    public NavigationView navigationView;
    public View content;
    @BindView(R.id.toolbar)
    public Toolbar toolbar;
    private static Bundle bundle = new Bundle();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        initToolbar();
        setupDrawerLayout();
        setFonts();

        notificationtoggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (notificationtoggle.isChecked()) {
                    PackageManager pm = SettingsActivity.this.getPackageManager();
                    ComponentName componentName = new ComponentName(
                            SettingsActivity.this, NetworkChangeReceiver.class);
                    pm.setComponentEnabledSetting(componentName,
                            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                            PackageManager.DONT_KILL_APP);
                    Toast.makeText(getApplicationContext(),
                            "Broadcast Receiver Started", Toast.LENGTH_SHORT)
                            .show();
                } else {
                    PackageManager pm = SettingsActivity.this.getPackageManager();
                    ComponentName componentName = new ComponentName(
                            SettingsActivity.this, NetworkChangeReceiver.class);
                    pm.setComponentEnabledSetting(componentName,
                            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                            PackageManager.DONT_KILL_APP);
                    Toast.makeText(getApplicationContext(),
                            "Broadcast Receiver Stopped", Toast.LENGTH_SHORT)
                            .show();
                }

            }

        });
    }

    private void setFonts() {
        FontUtils fontUtils = new FontUtils(getApplicationContext());
        fontUtils.changeFontTypeToNormal(tvDays);
        fontUtils.changeFontTypeToNormal(settingsTextview);
        fontUtils.changeFontTypeToNormal(tv_note);
        fontUtils.changeFontTypeToNormal(rb1);
        fontUtils.changeFontTypeToNormal(rb2);
        fontUtils.changeFontTypeToNormal(rb3);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayoutset.openDrawer(GravityCompat.START);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initToolbar() {
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_reorder);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public void setupDrawerLayout() {
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.drawer_home:
                        menuItem.setChecked(true);
                        Intent intent = new Intent(SettingsActivity.this, MainActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.drawer_more:
                        Snackbar.make(content, menuItem.getTitle() + " pressed", Snackbar.LENGTH_LONG).show();
                        menuItem.setChecked(true);
                        drawerLayoutset.closeDrawers();
                        return true;
                    case R.id.drawer_settings:
                        menuItem.setChecked(true);
                        drawerLayoutset.closeDrawers();
                        return true;
                }
                return onNavigationItemSelected(menuItem);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        notificationtoggle.setChecked(bundle.getBoolean("ToggleButtonState",false));
    }

    @Override
    protected void onPause() {
        super.onPause();
        bundle.putBoolean("ToggleButtonState", notificationtoggle.isChecked());
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


}