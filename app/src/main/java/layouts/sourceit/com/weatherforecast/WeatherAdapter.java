package layouts.sourceit.com.weatherforecast;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import layouts.sourceit.com.weatherforecast.models.MyResponse.Data.Request;
import layouts.sourceit.com.weatherforecast.models.MyResponse.Data.Weather;

class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.Viewholder> {

    interface WeatherClickListener {
        void onClick(Weather weather, Request request);
    }

    interface ItemClickListener {
        void onClick(int position);
    }

    private Context context;
    private List<Weather> weathers;
    private List<Request> request;
    private WeatherClickListener weatherClickListener;
    private ItemClickListener itemClickListener = new ItemClickListener() {
        @Override
        public void onClick(int position) {
            weatherClickListener.onClick(weathers.get(position), request.get(position));
        }
    };


    WeatherAdapter(List<Weather> weathers, List<Request> request, Context context, WeatherClickListener weatherClickListener) {
        this.weathers = weathers;
        this.request = request;
        this.context = context;
        this.weatherClickListener = weatherClickListener;

    }

    @Override
    public WeatherAdapter.Viewholder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.home, viewGroup, false);
        return new Viewholder(item, itemClickListener);

    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {
        Picasso.with(context)
                .load("http://cdn.worldweatheronline.net/images/wsymbols01_png_64/wsymbol_0001_sunny.png")
                .into(holder.iconWeather);

        Weather weather = weathers.get(position);
        holder.city2Textview.setText(request.get(0).query);
        holder.date.setText(weather.date);

        holder.weatherDesc.setText(weather.hourly.get(0).weatherDesc.get(0).value);
        holder.tempCText.setText(weather.hourly.get(0).tempC);
        holder.feelsLikeCText.setText(weather.hourly.get(0).FeelsLikeC);
        holder.pressureText.setText(weather.hourly.get(0).pressure);
        holder.rainPoss.setText(weather.hourly.get(0).chanceofrain);

        holder.tempCImg.setImageResource(R.mipmap.celcius);
        holder.feelsLikeCImg.setImageResource(R.mipmap.termometertwo);
        holder.pressureImg.setImageResource(R.mipmap.pressurethree);
        holder.rainPossImg.setImageResource(R.mipmap.drop);
    }

    @Override
    public int getItemCount() {
        return weathers.size();
    }

    class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.iconWeather)
        ImageView iconWeather;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.weatherDesc)
        TextView weatherDesc;
        @BindView(R.id.tempCText)
        TextView tempCText;
        @BindView(R.id.feelsLikeCText)
        TextView feelsLikeCText;
        @BindView(R.id.pressureText)
        TextView pressureText;
        @BindView(R.id.rainPoss)
        TextView rainPoss;
        @BindView(R.id.city2Textview)
        TextView city2Textview;

        @BindView(R.id.tempCImg)
        ImageView tempCImg;
        @BindView(R.id.feelsLikeCImg)
        ImageView feelsLikeCImg;
        @BindView(R.id.pressureImg)
        ImageView pressureImg;
        @BindView(R.id.rainPossImg)
        ImageView rainPossImg;

        ItemClickListener itemClickListener;

        Viewholder(View itemView, final ItemClickListener itemClickListener) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            this.itemClickListener = itemClickListener;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onClick(getAdapterPosition());
                }
            });
        }
    }
}