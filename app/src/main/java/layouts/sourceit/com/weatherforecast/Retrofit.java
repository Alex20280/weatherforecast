package layouts.sourceit.com.weatherforecast;

import layouts.sourceit.com.weatherforecast.models.MyResponse;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;

public class Retrofit {
    private static final String ENDPOINT = "http://api.worldweatheronline.com";
    private static ApiInterface apiInterface;

    static {
        initialize();

    }

    interface ApiInterface {
        @GET("/premium/v1/weather.ashx?key=f46c3e27281944ae9c755916173110&q=Kharkiv&format=json&num_of_days=30")
        void getData(Callback<MyResponse> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getData(Callback<MyResponse> callback) {
        apiInterface.getData(callback);
    }
}
