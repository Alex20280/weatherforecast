package layouts.sourceit.com.weatherforecast.models;

import java.io.Serializable;
import java.util.List;

public class MyResponse implements Serializable {

    public Data data;

    public static class Data {

        public List<Request> request;
        public List<Weather> weather;

        public static class Request implements Serializable {
            public String query; // Kharkiv, Ukraine
        }

        public static class Weather implements Serializable {
            public String date;
            public List<Astronomy> astronomy;
            public List<Hourly> hourly;

            public static class Astronomy implements Serializable {
                public String sunrise;
                public String sunset;
                public String moonrise;
                public String moonset;
            }

            public static class Hourly implements Serializable {
                public List<WeatherDesc> weatherDesc;
                public List<WeatherDesc> weatherIvonUrl;
                public String chanceofrain;
                public String pressure;
                public String tempC;
                public String FeelsLikeC;
                public String winddir16Point;
                public String windspeedKmph;
                public String humidity;


                public static class WeatherDesc implements Serializable {
                    public String value;
                }

                public static class WeatherIconUrl implements Serializable {
                    public String value;

                }
            }
        }
    }

}


